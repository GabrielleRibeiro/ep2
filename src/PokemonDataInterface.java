import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class PokemonDataInterface {

	private JFrame frame;
	private JButton buttonBack;
	private JLabel labelPokemonIdNumber;
	private JLabel labelPokemonsName;
	private JLabel labelLegendary;
	private JPanel panelOfPokemonType;
	private JPanel panelOfPokemonType1;
	private JLabel labelPokemonsFirstType;
	private JLabel labelPrimaryType;
	private JPanel panelPokemonType2;
	private JLabel labelPokemonSecondType;
	private JLabel labelSecondaryType;
	private JPanel panelOfPokemonFeatures;
	private JLabel labelExperience;
	private JLabel labelHeight;
	private JLabel labelWeight;
	private JLabel labelStatistics;
	private JPanel panelOfPokemonStatitics;
	private JLabel labelHp;
	private JLabel labelSpeed;
	private JLabel labelAttack;
	private JLabel labelDefense;
	private JLabel labelSpecialDefense;
	private JLabel labelSpecialAttack;
	private JLabel labelTotal;
	private JLabel labelAbilities;
	private JPanel panelOfPokemonAbilities;
	private JPanel panelOfPokemonAbility1;
	private JLabel labelPokemonAbility1;
	private JLabel labelAbility1;
	private JPanel panelOfPokemonAbility2;
	private JLabel labelPokemonAbility2;
	private JLabel labelAbility2;
	private JPanel panelOfPokemonAbility3;
	private JLabel labelPokemonAbility3;
	private JLabel labelAbility3;
	private JPanel panelOfPokemonMoves;
	private JPanel panelOfPokemonMove1;
	private JLabel labelMove1;
	private JPanel panelOfPokemonMove2;
	private JLabel labelMove2;
	private JPanel panelOfPokemonMove3;
	private JLabel labelMove3;
	private JPanel panelOfPokemonMove4;
	private JLabel labelMove4;
	private JPanel panelOfPokemonMove5;
	private JLabel labelMove5;
	private JPanel panelOfPokemonMove6;
	private JLabel labelMove6;
	private JPanel panelOfPokemonMove7;
	private JLabel labelMove7;
	private JLabel labelMoves;
	private JButton buttonCapture;
	private JLabel labelCaptured;
	private JLabel labelOfPokemonImage;
	private Pokemon pokemon = new Pokemon();
	private static Trainer trainer = null;
	private String previousPage;
	private JLabel labelOfBackgroundImage;
	private JLabel labelGeneration;
	
	
	public PokemonDataInterface() {
		
	}
	
	public Pokemon getPokemon() {
		return pokemon;
	}


	public void setPokemon(Pokemon pokemon) {
		this.pokemon = pokemon;
	}

	
	public static Trainer getTrainer() {
		return trainer;
	}


	public static void setTrainer(Trainer trainer) {
		PokemonDataInterface.trainer = trainer;
	}

	public String getPreviousPage() {
		return previousPage;
	}


	public void setPreviousPage(String previousPage) {
		this.previousPage = previousPage;
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		buttonBack = new JButton("Back");
		buttonBack.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 12));
		buttonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(previousPage == "GENERAL_POKEDEX") {
					PokemonSearchInterface pokemonSearchScreen = new PokemonSearchInterface();
					pokemonSearchScreen.openPage();
					frame.setVisible(false);
				}
				else if(previousPage == "TRAINER_POKEDEX") {
					TrainerPokedexInterface trainerPokedexScreen = new TrainerPokedexInterface();
					trainerPokedexScreen.openPage();
					frame.setVisible(false);
				}
				else if(previousPage == "SEARCH_BY_NAME") {
					PokemonSearchByNameInterface searchByNameScreen = new PokemonSearchByNameInterface();
					searchByNameScreen.openPage();
					frame.setVisible(false);
				}
				else if(previousPage ==  "SEARCH_BY_TYPE") {
					PokemonSearchByTypeInterface searchByTypeScreen = new PokemonSearchByTypeInterface();
					searchByTypeScreen.openPage();
					frame.setVisible(false);
				}
			}
		});
		buttonBack.setBounds(26, 26, 88, 25);
		buttonBack.setBackground(Color.decode("#f8e0d0"));
		frame.getContentPane().add(buttonBack);
		
		labelPokemonIdNumber = new JLabel("# " + pokemon.getIdNumber());
		labelPokemonIdNumber.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 18));
		labelPokemonIdNumber.setBounds(132, 32, 70, 32);
		frame.getContentPane().add(labelPokemonIdNumber);
		
		labelPokemonsName = new JLabel(pokemon.getName());
		labelPokemonsName.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 20));
		labelPokemonsName.setBounds(213, 31, 202, 32);
		frame.getContentPane().add(labelPokemonsName);
		
		if(pokemon.getLegendary().intern() == "True") {
			labelLegendary = new JLabel("Legendary");
			labelLegendary.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 15));
			labelLegendary.setBounds(214, 56, 118, 32);
			frame.getContentPane().add(labelLegendary);
		}
		
		panelOfPokemonType = new JPanel();
		panelOfPokemonType.setBackground(Color.pink.brighter());
		panelOfPokemonType.setBounds(26, 111, 352, 99);
		frame.getContentPane().add(panelOfPokemonType);
		panelOfPokemonType.setLayout(null);
		
		panelOfPokemonType1 = new JPanel();
		panelOfPokemonType1.setBackground(Color.decode("#f8e0d0"));
		panelOfPokemonType1.setBounds(27, 31, 132, 28);
		panelOfPokemonType.add(panelOfPokemonType1);
		
		labelPokemonsFirstType = new JLabel(pokemon.getType1());
		labelPokemonsFirstType.setFont(new Font("Noto Sans CJK KR Bold", Font.BOLD, 13));
		panelOfPokemonType1.add(labelPokemonsFirstType);
		
		labelPrimaryType = new JLabel("Primary Type");
		labelPrimaryType.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 12));
		labelPrimaryType.setBounds(55, 71, 92, 15);
		panelOfPokemonType.add(labelPrimaryType);
		
		if(pokemon.getType2().intern() != "") {
			panelPokemonType2 = new JPanel();
			panelPokemonType2.setBackground(Color.decode("#f8e0d0"));
			panelPokemonType2.setBounds(196, 31, 132, 28);
			panelOfPokemonType.add(panelPokemonType2);
			
			labelPokemonSecondType = new JLabel(pokemon.getType2());
			labelPokemonSecondType.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
			panelPokemonType2.add(labelPokemonSecondType);
			
			labelSecondaryType = new JLabel("Secondary Type");
			labelSecondaryType.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 12));
			labelSecondaryType.setBounds(206, 71, 113, 15);
			panelOfPokemonType.add(labelSecondaryType);
		}
		
		panelOfPokemonFeatures = new JPanel();
		panelOfPokemonFeatures.setBackground(Color.pink.brighter());
		panelOfPokemonFeatures.setBounds(390, 111, 179, 99);
		frame.getContentPane().add(panelOfPokemonFeatures);
		panelOfPokemonFeatures.setLayout(null);
		
		labelExperience = new JLabel("Experience: " + pokemon.getExperience());
		labelExperience.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		labelExperience.setBounds(12, 6, 181, 24);
		panelOfPokemonFeatures.add(labelExperience);
		
		labelHeight = new JLabel("Height: " + pokemon.getHeight());
		labelHeight.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		labelHeight.setBounds(12, 48, 171, 24);
		panelOfPokemonFeatures.add(labelHeight);
		
		labelWeight = new JLabel("Weight: " + pokemon.getWeight());
		labelWeight.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		labelWeight.setBounds(12, 25, 171, 24);
		panelOfPokemonFeatures.add(labelWeight);
		
		labelGeneration = new JLabel("Generation: " + pokemon.getGeneration());
		labelGeneration.setFont(new Font("Noto Sans CJK TC Bold", Font.BOLD, 13));
		labelGeneration.setBounds(12, 75, 155, 15);
		panelOfPokemonFeatures.add(labelGeneration);
		
		labelStatistics = new JLabel("Statistics");
		labelStatistics.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 15));
		labelStatistics.setBounds(26, 222, 108, 25);
		frame.getContentPane().add(labelStatistics);
		
		panelOfPokemonStatitics = new JPanel();
		panelOfPokemonStatitics.setBackground(Color.pink.brighter());
		panelOfPokemonStatitics.setBounds(26, 249, 547, 87);
		frame.getContentPane().add(panelOfPokemonStatitics);
		panelOfPokemonStatitics.setLayout(null);
		
		labelHp = new JLabel("HP: " + pokemon.getHp());
		labelHp.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		labelHp.setBounds(39, 12, 122, 15);
		panelOfPokemonStatitics.add(labelHp);
		
		labelSpeed = new JLabel("Speed: " + pokemon.getSpeed());
		labelSpeed.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		labelSpeed.setBounds(39, 35, 134, 15);
		panelOfPokemonStatitics.add(labelSpeed);
		
		labelAttack = new JLabel("Attack: " + pokemon.getAttack());
		labelAttack.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		labelAttack.setBounds(194, 12, 134, 15);
		panelOfPokemonStatitics.add(labelAttack);
		
		labelDefense = new JLabel("Defense: " + pokemon.getDefense());
		labelDefense.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		labelDefense.setBounds(194, 35, 134, 15);
		panelOfPokemonStatitics.add(labelDefense);
		
		labelSpecialDefense = new JLabel("Special Defense: " + pokemon.getSpecialDefense());
		labelSpecialDefense.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		labelSpecialDefense.setBounds(346, 35, 163, 15);
		panelOfPokemonStatitics.add(labelSpecialDefense);
		
		labelSpecialAttack = new JLabel("Special Attack: " + pokemon.getSpecialDefense());
		labelSpecialAttack.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		labelSpecialAttack.setBounds(346, 12, 163, 15);
		panelOfPokemonStatitics.add(labelSpecialAttack);
		
		labelTotal = new JLabel("Total: " + pokemon.getTotal());
		labelTotal.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		labelTotal.setBounds(346, 62, 149, 15);
		panelOfPokemonStatitics.add(labelTotal);
		
		labelAbilities = new JLabel("Abilities");
		labelAbilities.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 15));
		labelAbilities.setBounds(28, 348, 91, 15);
		frame.getContentPane().add(labelAbilities);
		
		panelOfPokemonAbilities = new JPanel();
		panelOfPokemonAbilities.setBackground(Color.pink.brighter());
		panelOfPokemonAbilities.setBounds(26, 375, 219, 183);
		frame.getContentPane().add(panelOfPokemonAbilities);
		panelOfPokemonAbilities.setLayout(null);
		
		if(pokemon.getAbilitie1().intern() != ""){	
			panelOfPokemonAbility1 = new JPanel();
			panelOfPokemonAbility1.setBackground(Color.decode("#f8e0d0"));
			panelOfPokemonAbility1.setBounds(12, 12, 195, 28);
			panelOfPokemonAbilities.add(panelOfPokemonAbility1);
			
			labelPokemonAbility1 = new JLabel(pokemon.getAbilitie1());
			labelPokemonAbility1.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
			panelOfPokemonAbility1.add(labelPokemonAbility1);
			
			labelAbility1 = new JLabel("Ability 1");
			labelAbility1.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 12));
			labelAbility1.setBounds(12, 38, 70, 28);
			panelOfPokemonAbilities.add(labelAbility1);
		}
			
		if(pokemon.getAbilitie2().intern() != "") {
			panelOfPokemonAbility2 = new JPanel();
			panelOfPokemonAbility2.setBackground(Color.decode("#f8e0d0"));
			panelOfPokemonAbility2.setBounds(12, 65, 195, 28);
			panelOfPokemonAbilities.add(panelOfPokemonAbility2);
			
			labelPokemonAbility2 = new JLabel(pokemon.getAbilitie2());
			labelPokemonAbility2.setFont(new Font("Noto Sans CJK TC Bold", Font.BOLD, 12));
			panelOfPokemonAbility2.add(labelPokemonAbility2);
			
			labelAbility2 = new JLabel("Ability 2");
			labelAbility2.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 12));
			labelAbility2.setBounds(12, 90, 70, 28);
			panelOfPokemonAbilities.add(labelAbility2);
		}
		
		if(pokemon.getAbilitie3().intern() != "") {
			panelOfPokemonAbility3 = new JPanel();
			panelOfPokemonAbility3.setBackground(Color.decode("#f8e0d0"));
			panelOfPokemonAbility3.setBounds(12, 117, 195, 28);
			panelOfPokemonAbilities.add(panelOfPokemonAbility3);
			
			labelPokemonAbility3 = new JLabel(pokemon.getAbilitie3());
			labelPokemonAbility3.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 12));
			panelOfPokemonAbility3.add(labelPokemonAbility3);
			
			labelAbility3 = new JLabel("Ability 3");
			labelAbility3.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 12));
			labelAbility3.setBounds(12, 149, 70, 15);
			panelOfPokemonAbilities.add(labelAbility3);
		}
		
		panelOfPokemonMoves = new JPanel();
		panelOfPokemonMoves.setBackground(Color.pink.brighter());
		panelOfPokemonMoves.setBounds(265, 375, 308, 183);
		frame.getContentPane().add(panelOfPokemonMoves);
		panelOfPokemonMoves.setLayout(null);
		
		if(pokemon.getMove1().intern() != "") {
			panelOfPokemonMove1 = new JPanel();
			panelOfPokemonMove1.setBackground(Color.decode("#f8e0d0"));
			panelOfPokemonMove1.setBounds(12, 12, 141, 22);
			panelOfPokemonMoves.add(panelOfPokemonMove1);
			
			labelMove1 = new JLabel(pokemon.getMove1());
			labelMove1.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 12));
			panelOfPokemonMove1.add(labelMove1);
		}
		
		if(pokemon.getMove2().intern() != "") {
			panelOfPokemonMove2 = new JPanel();
			panelOfPokemonMove2.setBackground(Color.decode("#f8e0d0"));
			panelOfPokemonMove2.setBounds(12, 52, 141, 22);
			panelOfPokemonMoves.add(panelOfPokemonMove2);
			
			labelMove2 = new JLabel(pokemon.getMove2());
			labelMove2.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 12));
			panelOfPokemonMove2.add(labelMove2);
		}
		
		if(pokemon.getMove3().intern() != "") {
			panelOfPokemonMove3 = new JPanel();
			panelOfPokemonMove3.setBackground(Color.decode("#f8e0d0"));
			panelOfPokemonMove3.setBounds(12, 93, 141, 22);
			panelOfPokemonMoves.add(panelOfPokemonMove3);
			
			labelMove3 = new JLabel(pokemon.getMove3());
			labelMove3.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 12));
			panelOfPokemonMove3.add(labelMove3);
		}
		
		if(pokemon.getMove4().intern() != "") {
			panelOfPokemonMove4 = new JPanel();
			panelOfPokemonMove4.setBackground(Color.decode("#f8e0d0"));
			panelOfPokemonMove4.setBounds(12, 132, 141, 22);
			panelOfPokemonMoves.add(panelOfPokemonMove4);
			
			labelMove4 = new JLabel(pokemon.getMove4());
			labelMove4.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 12));
			panelOfPokemonMove4.add(labelMove4);
		}
		
		if(pokemon.getMove5().intern() != "") {
			panelOfPokemonMove5 = new JPanel();
			panelOfPokemonMove5.setBackground(Color.decode("#f8e0d0"));
			panelOfPokemonMove5.setBounds(165, 12, 131, 22);
			panelOfPokemonMoves.add(panelOfPokemonMove5);
			
			labelMove5 = new JLabel(pokemon.getMove5());
			labelMove5.setFont(new Font("Noto Sans CJK TC Bold", Font.BOLD, 12));
			panelOfPokemonMove5.add(labelMove5);
		}
		
		if(pokemon.getMove6().intern() != "") {
			panelOfPokemonMove6 = new JPanel();
			panelOfPokemonMove6.setBackground(Color.decode("#f8e0d0"));
			panelOfPokemonMove6.setBounds(165, 52, 131, 22);
			panelOfPokemonMoves.add(panelOfPokemonMove6);
			
			labelMove6 = new JLabel(pokemon.getMove6());
			labelMove6.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 12));
			panelOfPokemonMove6.add(labelMove6);
		}
		
		if(pokemon.getMove7().intern() != "") {
			panelOfPokemonMove7 = new JPanel();
			panelOfPokemonMove7.setBackground(Color.decode("#f8e0d0"));
			panelOfPokemonMove7.setBounds(165, 93, 131, 22);
			panelOfPokemonMoves.add(panelOfPokemonMove7);
			
			labelMove7 = new JLabel(pokemon.getMove7());
			labelMove7.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 12));
			panelOfPokemonMove7.add(labelMove7);
		}
		
		labelMoves = new JLabel("Moves");
		labelMoves.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 15));
		labelMoves.setBounds(271, 348, 91, 15);
		frame.getContentPane().add(labelMoves);
		
		labelOfPokemonImage = new JLabel("");
		labelOfPokemonImage.setBounds(427, 26, 142, 83);
		Image pokemonImage = new ImageIcon(this.getClass().getResource(pokemon.getImageDirectory())).getImage();
		labelOfPokemonImage.setIcon(new ImageIcon(pokemonImage));
		frame.getContentPane().add(labelOfPokemonImage);
		
		if(trainer != null) {
			if(!trainer.verifyIfPokemonIsRegisteredInPersonalPokedex(pokemon)) {
				buttonCapture = new JButton("Capture");
				buttonCapture.setBackground(Color.decode("#f8e0d0"));
				buttonCapture.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 12));
				buttonCapture.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						trainer.addPokemonInPersonalPokedex(pokemon);
						JOptionPane.showMessageDialog(frame, pokemon.getName() + " was captured!");
						PokemonDataInterface pokemonDataScreen = new PokemonDataInterface();
						pokemonDataScreen.setPokemon(pokemon);
						pokemonDataScreen.setPreviousPage(previousPage);
						pokemonDataScreen.openPage();
						frame.setVisible(false);
					}
				});
				buttonCapture.setBounds(26, 63, 88, 25);
				frame.getContentPane().add(buttonCapture);
			}
			else {
				labelCaptured = new JLabel("Captured!");
				labelCaptured.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 14));
				labelCaptured.setBounds(36, 63, 78, 15);
				frame.getContentPane().add(labelCaptured);
			}
		}
		
		labelOfBackgroundImage = new JLabel("");
		labelOfBackgroundImage.setBounds(0, 0, 600, 600);
		frame.getContentPane().add(labelOfBackgroundImage);
		Image backgroundImage = new ImageIcon(this.getClass().getResource("backgroundImages/pokeballBackground.png")).getImage();
		labelOfBackgroundImage.setIcon(new ImageIcon(backgroundImage));
		
		frame.setVisible(true);
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public void openPage() {
		initialize();
	}
}