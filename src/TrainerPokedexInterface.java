import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

public class TrainerPokedexInterface{

	private JFrame frame;
	private JButton buttonBack;
	private JLabel labelTrainersName;
	private JScrollPane scrollPaneOfPokemonList;
	private JList<String> listOfPokemons;
	private JButton buttonViewInformation;
	private JButton buttonSearchByName;
	private JLabel labelSearchInYourPokedex;
	private JButton buttonSearchByType;
	private static Trainer trainer = null;
	private String selectedPokemon;
	private Pokedex pokedex = new Pokedex();
	private JLabel labelOfPokemonImage;
	private JLabel labelOfBackgroundImage;
	
	public TrainerPokedexInterface() {
		
	}

	public static Trainer getTrainer() {
		return trainer;
	}

	public static void setTrainer(Trainer trainer) {
		TrainerPokedexInterface.trainer = trainer;
	}

	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		pokedex.readPokemonData();
		
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		buttonBack = new JButton("Back");
		buttonBack.setBackground(Color.pink.brighter());
		buttonBack.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 12));
		buttonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainMenuInterface mainMenuScreen = new MainMenuInterface();
				mainMenuScreen.openPage();
				frame.setVisible(false);
			}
		});
		buttonBack.setBounds(27, 30, 67, 25);
		frame.getContentPane().add(buttonBack);
		
		labelTrainersName = new JLabel(trainer.getName() + "'s Pokedex");
		labelTrainersName.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 20));
		labelTrainersName.setBounds(186, 30, 343, 37);
		frame.getContentPane().add(labelTrainersName);
		
		scrollPaneOfPokemonList = new JScrollPane();
		scrollPaneOfPokemonList.setBackground(Color.pink.brighter());
		scrollPaneOfPokemonList.setBounds(27, 124, 287, 411);
		frame.getContentPane().add(scrollPaneOfPokemonList);
		
		DefaultListModel<String> trainerPokemonsList = new DefaultListModel<String>();
		ArrayList<String> pokemons = new ArrayList<String>();
		trainer.loadPersonalPokemonList();
		pokemons = trainer.getPersonalPokemonsList();
		for(int i = 0; i < pokemons.size(); i++) {
			trainerPokemonsList.addElement(pokemons.get(i));
		}
		listOfPokemons = new JList<String>(trainerPokemonsList);
		listOfPokemons.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
		listOfPokemons.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				selectedPokemon = listOfPokemons.getSelectedValue().toString();
				Pokemon pokemon = new Pokemon();
				pokemon = pokedex.pokemonSearchByName(selectedPokemon);
				Image pokemonImage = new ImageIcon(this.getClass().getResource(pokemon.getImageDirectory())).getImage();
				labelOfPokemonImage.setIcon(new ImageIcon(pokemonImage));
			}
		});
		scrollPaneOfPokemonList.setViewportView(listOfPokemons);
		
		buttonViewInformation = new JButton("View Information");
		buttonViewInformation.setBackground(Color.pink.brighter());
		buttonViewInformation.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonViewInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pokemon pokemon = new Pokemon();
				pokemon = pokedex.pokemonSearchByName(selectedPokemon);
				PokemonDataInterface pokemonDataScreen = new PokemonDataInterface();
				pokemonDataScreen.setPokemon(pokemon); 
				PokemonDataInterface.setTrainer(trainer);
				pokemonDataScreen.setPreviousPage("TRAINER_POKEDEX");
				pokemonDataScreen.openPage();
				frame.setVisible(false);
			}
		});
		buttonViewInformation.setBounds(367, 252, 191, 25);
		frame.getContentPane().add(buttonViewInformation);
		
		buttonSearchByName = new JButton("Search By Name");
		buttonSearchByName.setBackground(Color.pink.brighter());
		buttonSearchByName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PokemonSearchByNameInterface searchByNameScreen = new PokemonSearchByNameInterface();
				searchByNameScreen.setTrainer(trainer);
				PokemonSearchByNameInterface.setPreviousPage("TRAINER_POKEDEX");
				searchByNameScreen.openPage();
				frame.setVisible(false);
			}
		});
		buttonSearchByName.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonSearchByName.setBounds(367, 374, 191, 25);
		frame.getContentPane().add(buttonSearchByName);
		
		labelSearchInYourPokedex = new JLabel("Search in your Pokedex:");
		labelSearchInYourPokedex.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 14));
		labelSearchInYourPokedex.setBounds(367, 347, 191, 15);
		frame.getContentPane().add(labelSearchInYourPokedex);
		
		buttonSearchByType = new JButton("Search By Type");
		buttonSearchByType.setBackground(Color.pink.brighter());
		buttonSearchByType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PokemonSearchByTypeInterface searchByTypeScreen = new PokemonSearchByTypeInterface();
				PokemonSearchByTypeInterface.setTrainer(trainer);
				trainer.loadPersonalPokedex();
				PokemonSearchByTypeInterface.setPreviousPage("TRAINER_POKEDEX");
				searchByTypeScreen.openPage();
				frame.setVisible(false);
			}
		});
		buttonSearchByType.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonSearchByType.setBounds(367, 411, 191, 25);
		frame.getContentPane().add(buttonSearchByType);
		
		labelOfPokemonImage = new JLabel("");
		labelOfPokemonImage.setBounds(409, 145, 120, 116);
		frame.getContentPane().add(labelOfPokemonImage);
		
		labelOfBackgroundImage = new JLabel("");
		labelOfBackgroundImage.setBounds(0, 0, 600, 600);
		frame.getContentPane().add(labelOfBackgroundImage);
		Image backgroundImage = new ImageIcon(this.getClass().getResource("backgroundImages/pokeballBackground.png")).getImage();
		labelOfBackgroundImage.setIcon(new ImageIcon(backgroundImage));
		
		frame.setVisible(true);
	}
	
	public void openPage() {
		initialize();
	}
}