import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PokemonTest {

	public String idNumber;
	public String name;
	public String type1;
	public String type2;
	public String total;
	public String hp;
	public String attack;
	public String defense;
	public String specialAttack;
	public String specialDefense;
	public String speed;
	public String generation;
	public String legendary;
	public String experience;
	public String height;
	public String weight;
	public String abilitie1;
	public String abilitie2;
	public String abilitie3;
	public String move1;
	public String move2;
	public String move3;
	public String move4;
	public String move5;
	public String move6;
	public String move7;
	public String imageDirectory;
	public Pokemon pokemon;
	

	@BeforeEach
	public void beforeTests(){
		idNumber = "16";
		name = "Pidgey";
		type1 = "Normal";
		type2 = "Flying";
		total = "251";
		hp = "40";
		attack = "45";
		defense = "40";
		specialAttack = "35";
		specialDefense = "35";
		speed = "56";
		generation = "1";
		legendary = "False";
		experience = "50";
		height = "3";
		weight = "18";
		abilitie1 = "big-pecks";
		abilitie2 = "tangled-feet";
		abilitie3 = "keen-eye";
		move1 = "razor-wind";
		move2 = "gust";
		move3 = "wing-attack";
		move4 = "whirlwind";
		move5 = "fly";
		move6 = "sand-attack";
		move7 = "tackle";
		imageDirectory = "images/pidgey.png";
		
		pokemon = new Pokemon();
	}

	@Test
	void constructorTest() {	
		pokemon = new Pokemon(idNumber, name, type1, type2, total, hp, attack, defense, specialAttack,
							  specialDefense, speed, generation, legendary, experience, height, weight,
							  abilitie1, abilitie2, abilitie3, move1, move2, move3, move4, move5, move6,
							  move7, imageDirectory);
		assertEquals(idNumber, pokemon.getIdNumber());
		assertEquals(name, pokemon.getName());
		assertEquals(type1, pokemon.getType1());
		assertEquals(type2, pokemon.getType2());
		assertEquals(total, pokemon.getTotal());
		assertEquals(hp, pokemon.getHp());
		assertEquals(attack, pokemon.getAttack());
		assertEquals(defense, pokemon.getDefense());
		assertEquals(specialAttack, pokemon.getSpecialAttack());
		assertEquals(specialDefense, pokemon.getSpecialDefense());
		assertEquals(speed, pokemon.getSpeed());
		assertEquals(generation, pokemon.getGeneration());
		assertEquals(legendary, pokemon.getLegendary());
		assertEquals(experience, pokemon.getExperience());
		assertEquals(height, pokemon.getHeight());
		assertEquals(weight, pokemon.getWeight());
		assertEquals(abilitie1, pokemon.getAbilitie1());
		assertEquals(abilitie2, pokemon.getAbilitie2());
		assertEquals(abilitie3, pokemon.getAbilitie3());
		assertEquals(move1, pokemon.getMove1());
		assertEquals(move2, pokemon.getMove2());
		assertEquals(move3, pokemon.getMove3());
		assertEquals(move4, pokemon.getMove4());
		assertEquals(move5, pokemon.getMove5());
		assertEquals(move6, pokemon.getMove6());
		assertEquals(move7, pokemon.getMove7());
		assertEquals(imageDirectory, pokemon.getImageDirectory());
	}

	@AfterEach
	void afterTests() {
		pokemon = null;
	}

}
