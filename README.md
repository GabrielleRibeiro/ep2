# Pokedex

## Introduction

<p>In the Pokemon's world, there are creatures called Pokemon. These creatures have 
many characteristics, like movements, weight, height, attack, defense etc. They are organized
organized by type. </p>

<p>Humans, called by trainers, capture pokemons and use them to fight each other as a sport.</p>

<p>In this software, you can register as a trainer and view Pokémon information.</p>

## Functionalities

### Trainer Register and Trainer's Pokedex

<p>You can register yourself as a trainer.</p>
<p>You can search pokemons and add them to your own Pokedex.
To do this, only need to find the desired pokemon, open its information and press the "Capture"
button on the screen. After that, the pokemon will be in your personal Pokedex. </p>

### Pokemon Search

<p>The search for pokemons can be done being logged or not. 
In addition to the normal search, where all pokemons are listed, there are also two more 
specific searches: the search by name and the search by type. </p>
<p>In the search by name, after informing the name of the desired pokemon,
will be informed if the pokemon was found or not.</p>
<p>In search by type, after selecting the searched type, all pokemons of that type will be listed.</p>
<p>After any type of search, in case the searched pokemon was found, you will be able to see its information. </p>

## Note About How Data Is Save In This Project

<p>The data about the registered trainers are saved in a csv file, called TRAINERS_DATA.csv. </p>
<p>The data about the Pokémon captured by a trainer is saved in a csv file called TRAINERS_POKEDEX.csv. </p>
<p>This files are inside the folder csv_files (inside the folder data).</p>
