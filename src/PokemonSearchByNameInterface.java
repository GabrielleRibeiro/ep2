import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Image;

public class PokemonSearchByNameInterface {

	private JFrame frame;
	private JTextField textFieldOfPokemonName;
	private JLabel labelSearchPokemonBy;
	private JPanel panelOfBackground;
	private JLabel labelSearchResult;
	private JButton buttonSearch;
	private JButton buttonViewInformation;
	private JButton buttonBack;
	private Pokedex pokedex;
	private String wantedPokemonName;
	private Pokemon wantedPokemon;
	private boolean pokemonFound;
	private static Trainer trainer = null;
	private JLabel labelOfPokemonImage;
	private static String previousPage;
	private JLabel labelOfBackgroundImage;
	
	public PokemonSearchByNameInterface() {
		
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainer) {
		PokemonSearchByNameInterface.trainer = trainer;
	}

	public static String getPreviousPage() {
		return previousPage;
	}

	public static void setPreviousPage(String previousPage) {
		PokemonSearchByNameInterface.previousPage = previousPage;
	}

	/**
	 * Initialize the contents of the frame.
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		pokedex = new Pokedex();
		if(previousPage.intern() == "TRAINER_POKEDEX") {
			trainer.loadPersonalPokedex();
			pokedex = trainer.getPersonalPokedex();
		}
		else if(previousPage.intern() == "GENERAL_POKEDEX"){
			pokedex.readPokemonData();
		}
		wantedPokemon = new Pokemon();
		pokemonFound = false;
		
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		labelSearchPokemonBy = new JLabel("POKEMON SEARCH BY NAME");
		labelSearchPokemonBy.setFont(new Font("Noto Sans CJK TC Black", Font.BOLD, 20));
		labelSearchPokemonBy.setBounds(145, 30, 429, 20);
		frame.getContentPane().add(labelSearchPokemonBy);
		
		panelOfBackground = new JPanel();
		panelOfBackground.setBackground(Color.pink.brighter());
		panelOfBackground.setBounds(56, 136, 484, 385);
		frame.getContentPane().add(panelOfBackground);
		panelOfBackground.setLayout(null);
		
		labelSearchResult = new JLabel();
		labelSearchResult.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 14));
		labelSearchResult.setBounds(152, 67, 178, 15);
		panelOfBackground.add(labelSearchResult);
		
		textFieldOfPokemonName= new JTextField();
		textFieldOfPokemonName.setBounds(56, 89, 324, 22);
		frame.getContentPane().add(textFieldOfPokemonName);
		textFieldOfPokemonName.setColumns(10);
		
		buttonSearch = new JButton("Search");
		buttonSearch.setBackground(Color.pink.brighter());
		buttonSearch.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				wantedPokemonName = textFieldOfPokemonName.getText();
				if(pokedex.pokemonSearchByName(wantedPokemonName) != null) {
					wantedPokemon = pokedex.pokemonSearchByName(wantedPokemonName);
					labelSearchResult.setText("Pokemon found!");
					pokemonFound = true;
					Image pokemonImage = new ImageIcon(this.getClass().getResource(wantedPokemon.getImageDirectory())).getImage();
					labelOfPokemonImage.setIcon(new ImageIcon(pokemonImage));
				}
				else {
					labelSearchResult.setText("Pokemon not found!");
					pokemonFound = false;
				}
			}
		});
		buttonSearch.setBounds(392, 89, 148, 22);
		frame.getContentPane().add(buttonSearch);
		
		labelOfPokemonImage = new JLabel("");
		labelOfPokemonImage.setBounds(170, 110, 143, 154);
		panelOfBackground.add(labelOfPokemonImage);
		
		buttonViewInformation = new JButton("View Information");
		buttonViewInformation.setBackground(Color.decode("#f8e0d0"));
		buttonViewInformation.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonViewInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pokemonFound) {
					PokemonDataInterface pokemonData = new PokemonDataInterface();
					pokemonData.setPokemon(wantedPokemon);
					PokemonDataInterface.setTrainer(trainer);
					pokemonData.setPreviousPage("SEARCH_BY_NAME");
					pokemonData.openPage();
					frame.setVisible(false);
				}
			}
		});
		buttonViewInformation.setBounds(111, 276, 269, 25);
		panelOfBackground.add(buttonViewInformation);
		
		buttonBack = new JButton("Back");
		buttonBack.setBackground(Color.pink.brighter());
		buttonBack.setFont(new Font("Noto Sans CJK KR Bold", Font.PLAIN, 12));
		buttonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(previousPage.intern() == "TRAINER_POKEDEX") {
					TrainerPokedexInterface trainerPokedex = new TrainerPokedexInterface();
					trainerPokedex.openPage();
					frame.setVisible(false);
				}
				else if(previousPage.intern() == "GENERAL_POKEDEX"){
					PokemonSearchInterface pokemonSearchScreen = new PokemonSearchInterface();
					pokemonSearchScreen.openPage();
					frame.setVisible(false);
				}
			}
		});
		buttonBack.setBounds(56, 32, 67, 25);
		frame.getContentPane().add(buttonBack);
		
		labelOfBackgroundImage = new JLabel("");
		labelOfBackgroundImage.setBounds(0, 0, 600, 600);
		frame.getContentPane().add(labelOfBackgroundImage);
		Image backgroundImage = new ImageIcon(this.getClass().getResource("backgroundImages/pokeballBackground.png")).getImage();
		labelOfBackgroundImage.setIcon(new ImageIcon(backgroundImage));
		
		frame.setVisible(true);
	}
	
	public void openPage() {
		initialize();
	}
}