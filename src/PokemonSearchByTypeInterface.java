import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.Font;
import java.awt.Image;

public class PokemonSearchByTypeInterface {

	private JFrame frame;
	private JButton buttonBack;
	private JLabel labelPokemonSearchByType;
	private JPanel panelOfBackground;
	private JComboBox<String> comboBoxOfTypes;
	private JButton buttonSearch;
	private JScrollPane scrollPaneOfPokemonType;
	private JList<String> listOfPokemonType;
	private JButton buttonViewInformation;
	private Pokedex pokedex;
	private DefaultListModel<String> pokemonList = new DefaultListModel<String>();
	private String selectedPokemon;
	private String selectedPokemonType = "Select Type";
	private String[] listOfPokemonsTypes = new String[]{"Select Type", "Bug", "Dark",
														"Dragon", "Electric", "Fairy",
														"Fighting", "Fire", "Flying", 
														"Ghost", "Ground", "Grass",
														"Ice", "Normal", "Poison",
														"Psychic", "Rock", "Steel", "Water" };
	private static Trainer trainer = null;
	private JLabel labelOfPokemonImage;
	private Pokemon pokemon = new Pokemon();
	private static String previousPage;
	private JLabel labelOfBackgroundImage;
	
	public PokemonSearchByTypeInterface() {
		
	}

	public static Trainer getTrainer() {
		return trainer;
	}

	public static void setTrainer(Trainer trainer) {
		PokemonSearchByTypeInterface.trainer = trainer;
	}

	public static String getPreviousPage() {
		return previousPage;
	}

	public static void setPreviousPage(String previousPage) {
		PokemonSearchByTypeInterface.previousPage = previousPage;
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		pokedex = new Pokedex();
		if(previousPage.intern() == "TRAINER_POKEDEX") {
			pokedex = trainer.getPersonalPokedex();
		}
		else if(previousPage.intern() == "GENERAL_POKEDEX"){
			pokedex.readPokemonData();
		}
		
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		buttonBack = new JButton("Back");
		buttonBack.setBackground(Color.pink.brighter());
		buttonBack.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 12));
		buttonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(previousPage.intern() == "TRAINER_POKEDEX") {
					TrainerPokedexInterface trainerPokedexScreen = new TrainerPokedexInterface();
					trainerPokedexScreen.openPage();
					frame.setVisible(false);
				}
				else if(previousPage.intern() == "GENERAL_POKEDEX"){
					PokemonSearchInterface pokemonSearchScreen = new PokemonSearchInterface();
					pokemonSearchScreen.openPage();
					frame.setVisible(false);
				}
			}
		});
		buttonBack.setBounds(33, 12, 67, 25);
		frame.getContentPane().add(buttonBack);
		
		labelPokemonSearchByType = new JLabel("POKEMON SEARCH BY TYPE");
		labelPokemonSearchByType.setFont(new Font("Noto Sans CJK TC Black", Font.BOLD, 20));
		labelPokemonSearchByType.setBounds(144, 12, 415, 25);
		frame.getContentPane().add(labelPokemonSearchByType);
		
		panelOfBackground = new JPanel();
		panelOfBackground.setBackground(Color.pink.brighter());
		panelOfBackground.setBounds(30, 74, 527, 454);
		frame.getContentPane().add(panelOfBackground);
		panelOfBackground.setLayout(null);
		
		comboBoxOfTypes = new JComboBox<String>();
		comboBoxOfTypes.setBackground(Color.decode("#f5e3df"));
		comboBoxOfTypes.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
		comboBoxOfTypes.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				selectedPokemonType = comboBoxOfTypes.getSelectedItem().toString();
			}
		});
		comboBoxOfTypes.setBounds(43, 46, 261, 24);
		panelOfBackground.add(comboBoxOfTypes);
		for(int i = 0; i < listOfPokemonsTypes.length; i++) {
			comboBoxOfTypes.addItem(listOfPokemonsTypes[i]);
		}
		
		buttonSearch = new JButton("Search");
		buttonSearch.setBackground(Color.decode("#f8e0d0"));
		buttonSearch.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pokemonList.clear();
				ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
				pokemons.clear();
				pokemons = pokedex.pokemonSearchByType(selectedPokemonType); 
				for(int i = 0; i < pokemons.size(); i++) {
					pokemonList.addElement(pokemons.get(i).getName());
				}
			}
		});
		buttonSearch.setBounds(326, 46, 117, 24);
		panelOfBackground.add(buttonSearch);
		
		scrollPaneOfPokemonType = new JScrollPane();
		scrollPaneOfPokemonType.setBounds(43, 104, 225, 300);
		panelOfBackground.add(scrollPaneOfPokemonType);
		
		labelOfPokemonImage = new JLabel("");
		labelOfPokemonImage.setBounds(331, 123, 136, 137);
		panelOfBackground.add(labelOfPokemonImage);
		
		listOfPokemonType = new JList<String>(pokemonList);
		listOfPokemonType.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
		listOfPokemonType.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				selectedPokemon = listOfPokemonType.getSelectedValue().toString();
				pokemon = pokedex.pokemonSearchByName(selectedPokemon);
				Image pokemonImage = new ImageIcon(this.getClass().getResource(pokemon.getImageDirectory())).getImage();
				labelOfPokemonImage.setIcon(new ImageIcon(pokemonImage));
			}
		});
		scrollPaneOfPokemonType.setViewportView(listOfPokemonType);
		
		buttonViewInformation = new JButton("View Information");
		buttonViewInformation.setBackground(Color.decode("#f8e0d0"));
		buttonViewInformation.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonViewInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pokemon = pokedex.pokemonSearchByName(selectedPokemon);
				PokemonDataInterface pokemonDataScreen = new PokemonDataInterface(); 
				PokemonDataInterface.setTrainer(trainer);
				pokemonDataScreen.setPokemon(pokemon); 
				pokemonDataScreen.setPreviousPage("SEARCH_BY_TYPE");
				pokemonDataScreen.openPage();
				frame.setVisible(false);
			}
		});
		buttonViewInformation.setBounds(296, 254, 199, 25);
		panelOfBackground.add(buttonViewInformation);
		
		labelOfBackgroundImage = new JLabel("");
		labelOfBackgroundImage.setBounds(0, 0, 600, 600);
		frame.getContentPane().add(labelOfBackgroundImage);
		Image backgroundImage = new ImageIcon(this.getClass().getResource("backgroundImages/pokeballBackground.png")).getImage();
		labelOfBackgroundImage.setIcon(new ImageIcon(backgroundImage));
		
		frame.setVisible(true);
	}
	
	public void openPage() {
		initialize();
	}
}