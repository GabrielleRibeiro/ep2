import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;

public class MainMenuInterface {

	private JFrame frame;
	private JPanel panelOfBackground;
	private JButton buttonViewPokedex;
	private JButton buttonSearchPokemons;
	private JButton buttonLogOut;
	private JLabel labelMessage;
	private JPanel panelOfTrainerInformation;
	private JLabel labelTrainerName;
	private JLabel labelBirthday;
	private JLabel labelGender;
	private JLabel labelMainMenu;
	private static Trainer trainer = new Trainer();
	private JLabel label;
	
	public MainMenuInterface() {
		
	}

	public Trainer getTrainer() {
		return trainer;
	}

	public void setTrainer(Trainer trainer) {
		MainMenuInterface.trainer = trainer;
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		labelMainMenu = new JLabel("MAIN MENU");
		labelMainMenu.setBounds(219, 59, 278, 30);
		frame.getContentPane().add(labelMainMenu);
		labelMainMenu.setFont(new Font("Noto Sans CJK TC Black", Font.BOLD, 20));
		
		panelOfBackground = new JPanel();
		panelOfBackground.setBackground(Color.pink.brighter());
		panelOfBackground.setBounds(50, 315, 500, 227);
		frame.getContentPane().add(panelOfBackground);
		panelOfBackground.setLayout(null);
		
		buttonViewPokedex = new JButton("View your Pokedex");
		buttonViewPokedex.setBackground(Color.decode("#f8e0d0"));
		buttonViewPokedex.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonViewPokedex.setBounds(139, 76, 223, 25);
		buttonViewPokedex.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TrainerPokedexInterface trainerPokedexScreen = new TrainerPokedexInterface();
				TrainerPokedexInterface.setTrainer(trainer);
				trainerPokedexScreen.openPage();
				frame.setVisible(false);
			}
		});
		panelOfBackground.add(buttonViewPokedex);
		
		buttonSearchPokemons = new JButton("Search Pokemons");
		buttonSearchPokemons.setBackground(Color.decode("#f8e0d0"));
		buttonSearchPokemons.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonSearchPokemons.setBounds(139, 113, 223, 25);
		buttonSearchPokemons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokemonSearchInterface pokemonSearchScreen = new PokemonSearchInterface("MAINMENU");
				PokemonSearchInterface.setTrainer(trainer);
				pokemonSearchScreen.openPage();
				frame.setVisible(false);
			}
		});
		panelOfBackground.add(buttonSearchPokemons);
		
		buttonLogOut = new JButton("Log Out");
		buttonLogOut.setBackground(Color.decode("#f8e0d0"));
		buttonLogOut.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonLogOut.setBounds(139, 150, 223, 25);
		buttonLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginInterface loginScreen = new LoginInterface();
				loginScreen.openPage();
				frame.setVisible(false);
			}
		});
		panelOfBackground.add(buttonLogOut);
		
		labelMessage = new JLabel("Hi, " + trainer.getName() + "! What do you want to do?" );
		labelMessage.setFont(new Font("Noto Sans CJK TC Black", Font.PLAIN, 18));
		labelMessage.setBounds(12, 28, 476, 36);
		panelOfBackground.add(labelMessage);
		
		panelOfTrainerInformation = new JPanel();
		panelOfTrainerInformation.setBackground(Color.pink.brighter());
		panelOfTrainerInformation.setBounds(50, 101, 500, 202);
		frame.getContentPane().add(panelOfTrainerInformation);
		panelOfTrainerInformation.setLayout(null);
		
		labelTrainerName = new JLabel("Trainer: " + trainer.getName());
		labelTrainerName.setFont(new Font("Noto Sans CJK TC Bold", Font.BOLD, 15));
		labelTrainerName.setBounds(12, 12, 325, 29);
		panelOfTrainerInformation.add(labelTrainerName);
		
		labelBirthday = new JLabel("Birthday: " + trainer.getBirthdayMonth() +
										  " " + trainer.getBirthday() + ", " +
										  trainer.getBirthdayYear());
		labelBirthday.setFont(new Font("Noto Sans CJK TC Bold", Font.BOLD, 15));
		labelBirthday.setBounds(12, 50, 325, 29);
		panelOfTrainerInformation.add(labelBirthday);
		
		labelGender = new JLabel("Gender: " + trainer.getGender());
		labelGender.setFont(new Font("Noto Sans CJK TC Bold", Font.BOLD, 15));
		labelGender.setBounds(12, 91, 335, 29);
		panelOfTrainerInformation.add(labelGender);
		
		label = new JLabel("");
		label.setBounds(292, 0, 208, 202);
		panelOfTrainerInformation.add(label);
		Image pokemonImage = new ImageIcon(this.getClass().getResource("backgroundImages/pikachuInPokeball.gif")).getImage();
		label.setIcon(new ImageIcon(pokemonImage));
		
		JLabel labelOfBackgroundImage = new JLabel("");
		labelOfBackgroundImage.setBounds(0, 0, 600, 600);
		frame.getContentPane().add(labelOfBackgroundImage);
		Image backgroundImage = new ImageIcon(this.getClass().getResource("backgroundImages/pokeballBackground.png")).getImage();
		labelOfBackgroundImage.setIcon(new ImageIcon(backgroundImage));
		
		frame.setVisible(true);
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public void openPage() {
		initialize();
	}
}