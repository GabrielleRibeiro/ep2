import javax.swing.JFrame;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JLabel;

public class PokemonSearchInterface {

	private JFrame frame;
	private JButton buttonBack;
	private JList<String> listOfPokemons;
	private JScrollPane scrollPaneOfPokemonList;
	private JButton buttonSearchByName;
	private JButton buttonSearchByType;
	private JButton buttonViewPokemonsInformation;
	private Pokedex generalPokedex = new Pokedex();
	private String selectedPokemon;
	private static String previousPage;
	private static Trainer trainer = null;
	private JLabel labelPokemonSearch;
	private JLabel labelOfPokemonImage;
	private JLabel labelOfBackgroundImage;

	public PokemonSearchInterface() {
		
	}
	
	public PokemonSearchInterface(String previousPage) {
		PokemonSearchInterface.previousPage = previousPage;
	}
	
	public String getPreviousPage() {
		return previousPage;
	}

	public void setPreviousPage(String previousPage) {
		PokemonSearchInterface.previousPage = previousPage;
	}

	public static Trainer getTrainer() {
		return trainer;
	}

	public static void setTrainer(Trainer trainer) {
		PokemonSearchInterface.trainer = trainer;
	}

	private void initialize() {
		generalPokedex.readPokemonData();
		
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		buttonBack = new JButton("Back");
		buttonBack.setBackground(Color.pink.brighter());
		buttonBack.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 12));
		buttonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(previousPage.intern() == "LOGIN") {
					LoginInterface loginScreen = new LoginInterface();
					loginScreen.openPage();
					frame.setVisible(false);
				}
				else if(previousPage.intern() == "MAINMENU") {
					MainMenuInterface mainMenuScreen = new MainMenuInterface();
					mainMenuScreen.openPage();
					frame.setVisible(false);
				}
				
			}
		});
		buttonBack.setBounds(27, 25, 81, 25);
		frame.getContentPane().add(buttonBack);
		
		labelOfPokemonImage = new JLabel("");
		labelOfPokemonImage.setBounds(407, 191, 137, 142);
		frame.getContentPane().add(labelOfPokemonImage);
		
		ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
		DefaultListModel<String> pokemonList = new DefaultListModel<String>();
		pokemons = generalPokedex.getPokemons();
		for(int i = 1; i < pokemons.size(); i++) {
			pokemonList.addElement(pokemons.get(i).getName());
		}
		listOfPokemons = new JList<String>(pokemonList);
		listOfPokemons.setFont(new Font("DejaVu Serif", Font.BOLD, 14));
		listOfPokemons.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (listOfPokemons.getSelectedValue() != null) {
					selectedPokemon = listOfPokemons.getSelectedValue().toString();
					Pokemon pokemon = new Pokemon();
					pokemon = generalPokedex.pokemonSearchByName(selectedPokemon);
					
					Image pokemonImage = new ImageIcon(this.getClass().getResource(pokemon.getImageDirectory())).getImage();
					labelOfPokemonImage.setIcon(new ImageIcon(pokemonImage));
					
				}
			}
		});
		frame.getContentPane().add(listOfPokemons);
		listOfPokemons.setBounds(24, 194, 291, 340);

		scrollPaneOfPokemonList = new JScrollPane(listOfPokemons);
		scrollPaneOfPokemonList.setBounds(27, 166, 306, 329);
		frame.getContentPane().add(scrollPaneOfPokemonList);
		
		buttonSearchByName = new JButton("Search by Name");
		buttonSearchByName.setBackground(Color.pink.brighter());
		buttonSearchByName.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonSearchByName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokemonSearchByNameInterface pokemonSearchScreen = new PokemonSearchByNameInterface();
				PokemonSearchByNameInterface.setPreviousPage("GENERAL_POKEDEX");
				pokemonSearchScreen.setTrainer(trainer);
				pokemonSearchScreen.openPage();
				frame.setVisible(false);
			}
		});
		buttonSearchByName.setBounds(142, 73, 298, 25);
		frame.getContentPane().add(buttonSearchByName);
		
		buttonSearchByType = new JButton("Search by Type");
		buttonSearchByType.setBackground(Color.pink.brighter());
		buttonSearchByType.setFont(new Font("Noto Sans CJK TC Bold", Font.PLAIN, 13));
		buttonSearchByType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PokemonSearchByTypeInterface pokemonSearchScreen = new PokemonSearchByTypeInterface();
				PokemonSearchByTypeInterface.setTrainer(trainer);
				PokemonSearchByTypeInterface.setPreviousPage("GENERAL_POKEDEX");
				pokemonSearchScreen.openPage();
				frame.setVisible(false);
			}
		});
		buttonSearchByType.setBounds(142, 110, 298, 25);
		frame.getContentPane().add(buttonSearchByType);
		
		buttonViewPokemonsInformation = new JButton("View Information");
		buttonViewPokemonsInformation.setBackground(Color.pink.brighter());
		buttonViewPokemonsInformation.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonViewPokemonsInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Pokemon pokemon = new Pokemon();
				pokemon = generalPokedex.pokemonSearchByName(selectedPokemon);
				PokemonDataInterface pokemonDataScreen = new PokemonDataInterface(); // Mudar construtor
				pokemonDataScreen.setPokemon(pokemon); 
				pokemonDataScreen.setPreviousPage("GENERAL_POKEDEX");
				PokemonDataInterface.setTrainer(trainer);
				pokemonDataScreen.openPage();
				frame.setVisible(false);
			}
		});
		buttonViewPokemonsInformation.setBounds(362, 345, 206, 25);
		frame.getContentPane().add(buttonViewPokemonsInformation);
		
		labelPokemonSearch = new JLabel("POKEMON SEARCH");
		labelPokemonSearch.setFont(new Font("Noto Sans CJK TC Black", Font.BOLD, 20));
		labelPokemonSearch.setBounds(193, 23, 351, 20);
		frame.getContentPane().add(labelPokemonSearch);
		
		labelOfBackgroundImage = new JLabel("");
		labelOfBackgroundImage.setBounds(0, 0, 600, 600);
		frame.getContentPane().add(labelOfBackgroundImage);
		Image backgroundImage = new ImageIcon(this.getClass().getResource("backgroundImages/pokeballBackground.png")).getImage();
		labelOfBackgroundImage.setIcon(new ImageIcon(backgroundImage));
		
		frame.setVisible(true);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public void openPage() {
		initialize();
	}
}