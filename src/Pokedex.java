import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Pokedex {
	
	private ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
	
	public Pokedex() {
		
	}
	
	public ArrayList<Pokemon> getPokemons(){
		return pokemons;
	}
	
	public Pokemon pokemonSearchByName(String name) {
		for(int i = 0; i < pokemons.size(); i++) {
			if(name.toLowerCase().intern() == pokemons.get(i).getName().toLowerCase().intern()) {
				return pokemons.get(i);
			}
		}
		return null;
	}
	
	public ArrayList<Pokemon> pokemonSearchByType(String type) {
		ArrayList<Pokemon> pokemonsOfTheSameType = new ArrayList<Pokemon>();
		for(int i = 0; i < pokemons.size(); i++) {
			if(type.intern() == pokemons.get(i).getType1().intern() || type.intern() == pokemons.get(i).getType2().intern()) {
				pokemonsOfTheSameType.add(pokemons.get(i));
			}
		}
		return pokemonsOfTheSameType;
	}
	
	public void readPokemonData() {
		String csvFile = "data/csv_files/POKEMONS_DATA_1.csv";
		String line;
		String csvSplitBy = ",";	
		
		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {
            	   Pokemon pokemon = new Pokemon();
            	
            	   String[] pokemonAtribute = line.split(csvSplitBy);
            	   
	               pokemon.setIdNumber(pokemonAtribute[0]);
	               pokemon.setName(pokemonAtribute[1]);
	               pokemon.setType1(pokemonAtribute[2]);
	               pokemon.setType2(pokemonAtribute[3]);
	               pokemon.setTotal(pokemonAtribute[4]);
	               pokemon.setHp(pokemonAtribute[5]);
	               pokemon.setAttack(pokemonAtribute[6]);
	               pokemon.setDefense(pokemonAtribute[7]);
	               pokemon.setSpecialAttack(pokemonAtribute[8]);
	               pokemon.setSpecialDefense(pokemonAtribute[9]);
	               pokemon.setSpeed(pokemonAtribute[10]);
	               pokemon.setGeneration(pokemonAtribute[11]);
	               pokemon.setLegendary(pokemonAtribute[12]);
	                
	               pokemons.add(pokemon);	               
	        }
            
            csvFile = "data/csv_files/POKEMONS_DATA_2.csv";
            
            try(BufferedReader br2 = new BufferedReader(new FileReader(csvFile))){
            	
            	int counter = 0;
           
            	while((line = br2.readLine()) != null) {
            		String[] pokemonAtributeFromSecondCSV = line.split(csvSplitBy);
            		
            		for(int i = 2; i < pokemonAtributeFromSecondCSV.length; i++) {
            			switch(i) {
            				case 2:
            					pokemons.get(counter).setExperience(pokemonAtributeFromSecondCSV[2]);
            					break;
            				case 3:
            					pokemons.get(counter).setHeight(pokemonAtributeFromSecondCSV[3]);
            					break;
            				case 4:
            					pokemons.get(counter).setWeight(pokemonAtributeFromSecondCSV[4]);
            					break;
            				case 5:
            					pokemons.get(counter).setAbilitie1(pokemonAtributeFromSecondCSV[5]);
            					break;
            				case 6:
            					pokemons.get(counter).setAbilitie2(pokemonAtributeFromSecondCSV[6]);
            					break;
            				case 7:
            					pokemons.get(counter).setAbilitie3(pokemonAtributeFromSecondCSV[7]);
            					break;
            				case 8:
            					pokemons.get(counter).setMove1(pokemonAtributeFromSecondCSV[8]);
            					break;
            				case 9:
            					pokemons.get(counter).setMove2(pokemonAtributeFromSecondCSV[9]);
            					break;
            				case 10:
            					pokemons.get(counter).setMove3(pokemonAtributeFromSecondCSV[10]);
            					break;
            				case 11:
            					pokemons.get(counter).setMove4(pokemonAtributeFromSecondCSV[11]);
            					break;
            				case 12:
            					pokemons.get(counter).setMove5(pokemonAtributeFromSecondCSV[12]);
            					break;
            				case 13:
            					pokemons.get(counter).setMove6(pokemonAtributeFromSecondCSV[13]);
            					break;
            				case 14:
            					pokemons.get(counter).setMove7(pokemonAtributeFromSecondCSV[14]);
            					break;
            			}
            		}
            		counter++;
            	}
            }catch(IOException e) {
  	          e.printStackTrace();
  	      	}        
            
	     } catch (IOException e) {
	          e.printStackTrace();
	      }
		
		for(int i = 1; i < pokemons.size(); i++) {
			String imageDirectory = "images/" + pokemons.get(i).getName().toLowerCase() + ".png";
			pokemons.get(i).setImageDirectory(imageDirectory);
		}
		
		for(int i = 1; i < pokemons.size(); i++) {
			String pokemonName;
			String newPokemonName = "";
			boolean firstUpperLetter = false;
			
			pokemonName = pokemons.get(i).getName();
			for(int j = 0; j < pokemonName.length(); j++) {
				if(pokemonName.charAt(j) >= 'A' && pokemonName.charAt(j) <= 'Z' && !firstUpperLetter) {
					newPokemonName += pokemonName.charAt(j);
					firstUpperLetter = true;
				}
				else if(pokemonName.charAt(j) >= 'A' && pokemonName.charAt(j) <= 'Z' && firstUpperLetter) {
					newPokemonName += " " + pokemonName.charAt(j);
				}
				else {
					newPokemonName += pokemonName.charAt(j);
				}
			}
			pokemons.get(i).setName(newPokemonName);
		}
	}
	
	public void addPokemon(Pokemon pokemon) {
		pokemons.add(pokemon);
	}
	
	public void clearPokedex() {
		pokemons.clear();
	}	
}