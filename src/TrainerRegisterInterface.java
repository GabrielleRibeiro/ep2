import javax.swing.JFrame;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

public class TrainerRegisterInterface {

	private JFrame frame;
	private JTextField textFieldOfName;
	private JTextField textFieldOfEmail;
	private JComboBox<String> comboBoxOfMonth; 
	private JComboBox<String> comboBoxOfDay;
	private JComboBox<String> comboBoxOfYear;
	private JRadioButton radioButtonOfFemaleGender;
	private JRadioButton radioButtonOfMaleGender;
	private JRadioButton radioButtonOfOtherGender;
	private ButtonGroup buttonGroupOfGender;
	private JPasswordField passwordField;
	private JPasswordField passwordFieldToConfirmPassword;
	private JLabel labelName;
	private JLabel labelEmail;
	private JLabel labelBirthday;
	private JLabel labelGender;
	private JLabel labelPassword;
	private JLabel labelConfirmPassword;
	private JButton buttonBack;
	private JButton buttonConfirm;
	private JLabel labelOfBackgroundImage;
	private String[] months = new String[]{"January", "February", "March", "April", "May", "June",
											"July", "August", "September", "October", "November", "December"};
	private String[] days = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15",
										 "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
	private String[] years = new String[]{"1950", "1951", "1952", "1953", "1954", "1955", "1956", "1957", "1958", "1959",
										  "1960", "1961", "1962", "1963", "1964", "1965", "1966", "1967", "1968", "1969",
									 	  "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979",
										  "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989",
										  "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999",
										  "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009",
										  "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"};
	private String trainerName;
	private String trainerEmail;
	private String selectedMonth;
	private String selectedDay;
	private String selectedYear;
	private String selectedGender;
	private String trainerPassword;
	private String trainerConfirmPassword;
	private JLabel lblTrainersRegister;
	private JLabel labelOfImage;
	
	public TrainerRegisterInterface() {
		
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("DejaVu Serif", Font.PLAIN, 12));
		frame.setBounds(600, 600, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		textFieldOfName = new JTextField();
		textFieldOfName.setBounds(225, 99, 242, 23);
		frame.getContentPane().add(textFieldOfName);
		textFieldOfName.setColumns(10);
		
		textFieldOfEmail = new JTextField();
		textFieldOfEmail.setBounds(225, 134, 242, 23);
		frame.getContentPane().add(textFieldOfEmail);
		textFieldOfEmail.setColumns(10);
		
		comboBoxOfMonth = new JComboBox<String>();
		comboBoxOfMonth.setBackground(Color.pink.brighter());
		comboBoxOfMonth.setFont(new Font("DejaVu Serif", Font.BOLD, 12));
		comboBoxOfMonth.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				selectedMonth = comboBoxOfMonth.getSelectedItem().toString();
			}
		});
		comboBoxOfMonth.setBounds(225, 169, 107, 19);
		frame.getContentPane().add(comboBoxOfMonth);
		for(int i = 0; i < months.length; i++) {
			comboBoxOfMonth.addItem(months[i]);
		}
		
		comboBoxOfDay = new JComboBox<String>();
		comboBoxOfDay.setBackground(Color.pink.brighter());
		comboBoxOfDay.setFont(new Font("DejaVu Serif", Font.BOLD, 12));
		comboBoxOfDay.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				selectedDay = comboBoxOfDay.getSelectedItem().toString();
			}
		});
		comboBoxOfDay.setBounds(344, 169, 51, 19);
		frame.getContentPane().add(comboBoxOfDay);
		for(int i = 0; i < days.length; i++) {
			comboBoxOfDay.addItem(days[i]);
		}
		
		comboBoxOfYear = new JComboBox<String>();
		comboBoxOfYear.setBackground(Color.pink.brighter());
		comboBoxOfYear.setFont(new Font("DejaVu Serif", Font.BOLD, 12));
		comboBoxOfYear.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				selectedYear = comboBoxOfYear.getSelectedItem().toString();
			}
		});
		comboBoxOfYear.setBounds(407, 169, 60, 19);
		frame.getContentPane().add(comboBoxOfYear);
		for(int i = 0; i < years.length; i++) {
			comboBoxOfYear.addItem(years[i]);
		}
		
		radioButtonOfFemaleGender = new JRadioButton("Female");
		radioButtonOfFemaleGender.setBackground(Color.pink.brighter());
		radioButtonOfFemaleGender.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		radioButtonOfFemaleGender.setBounds(225, 196, 76, 23);
		frame.getContentPane().add(radioButtonOfFemaleGender);
		
		radioButtonOfMaleGender = new JRadioButton("Male");
		radioButtonOfMaleGender.setBackground(Color.pink.brighter()); 
		radioButtonOfMaleGender.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		radioButtonOfMaleGender.setBounds(320, 196, 60, 23);
		frame.getContentPane().add(radioButtonOfMaleGender);
		
		radioButtonOfOtherGender = new JRadioButton("Other");
		radioButtonOfOtherGender.setBackground(Color.pink.brighter());
		radioButtonOfOtherGender.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		radioButtonOfOtherGender.setBounds(391, 196, 76, 23);
		frame.getContentPane().add(radioButtonOfOtherGender);
		
		buttonGroupOfGender = new ButtonGroup();
		buttonGroupOfGender.add(radioButtonOfFemaleGender);
		buttonGroupOfGender.add(radioButtonOfMaleGender);
		buttonGroupOfGender.add(radioButtonOfOtherGender);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(225, 236, 242, 23);
		frame.getContentPane().add(passwordField);
		
		passwordFieldToConfirmPassword = new JPasswordField();
		passwordFieldToConfirmPassword.setBounds(225, 271, 242, 23);
		frame.getContentPane().add(passwordFieldToConfirmPassword);
		
		labelName = new JLabel("NAME:");
		labelName.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 13));
		labelName.setBounds(154, 102, 70, 15);
		frame.getContentPane().add(labelName);
		
		labelEmail = new JLabel("EMAIL:");
		labelEmail.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 13));
		labelEmail.setBounds(154, 137, 70, 15);
		frame.getContentPane().add(labelEmail);
		
		labelBirthday = new JLabel("BIRTHDAY:");
		labelBirthday.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 13));
		labelBirthday.setBounds(121, 170, 135, 15);
		frame.getContentPane().add(labelBirthday);
		
		labelGender = new JLabel("GENDER:");
		labelGender.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 13));
		labelGender.setBounds(137, 200, 87, 15);
		frame.getContentPane().add(labelGender);
		
		labelPassword = new JLabel("PASSWORD:");
		labelPassword.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 13));
		labelPassword.setBounds(117, 239, 107, 15);
		frame.getContentPane().add(labelPassword);
		
		labelConfirmPassword = new JLabel("CONFIRM PASSWORD:");
		labelConfirmPassword.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 13));
		labelConfirmPassword.setBounds(48, 274, 169, 15);
		frame.getContentPane().add(labelConfirmPassword);
		
		buttonBack = new JButton("Back");
		buttonBack.setBackground(Color.pink.brighter());
		buttonBack.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		buttonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginInterface login = new LoginInterface();
				login.openPage();
				frame.setVisible(false);
			}
		});
		buttonBack.setBounds(152, 306, 117, 25);
		frame.getContentPane().add(buttonBack);
		
		buttonConfirm = new JButton("Confirm");
		buttonConfirm.setBackground(Color.pink.brighter());
		buttonConfirm.setFont(new Font("Noto Sans CJK SC Bold", Font.BOLD, 13));
		buttonConfirm.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				trainerName = textFieldOfName.getText();
				trainerEmail = textFieldOfEmail.getText();
				if(radioButtonOfFemaleGender.isSelected()){
					selectedGender = "Female";
				}
				else if(radioButtonOfMaleGender.isSelected()) {
					selectedGender = "Male";
				}
				else if(radioButtonOfOtherGender.isSelected()) {
					selectedGender = "Other";
				}
				trainerPassword = passwordField.getText();
				trainerConfirmPassword = passwordFieldToConfirmPassword.getText();
				registerTrainer();
			}
		});
		buttonConfirm.setBounds(304, 306, 117, 25);
		frame.getContentPane().add(buttonConfirm);
		
		lblTrainersRegister = new JLabel("TRAINER'S REGISTER");
		lblTrainersRegister.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 20));
		lblTrainersRegister.setBounds(182, 47, 312, 40);
		frame.getContentPane().add(lblTrainersRegister);
		
		labelOfImage = new JLabel("");
		labelOfImage.setBounds(206, 351, 207, 221);
		frame.getContentPane().add(labelOfImage);
		Image Image = new ImageIcon(this.getClass().getResource("backgroundImages/pikachuCute.gif")).getImage();
		labelOfImage.setIcon(new ImageIcon(Image));
		
		labelOfBackgroundImage = new JLabel("");
		labelOfBackgroundImage.setBounds(0, 0, 600, 600);
		frame.getContentPane().add(labelOfBackgroundImage);
		Image backgroundImage = new ImageIcon(this.getClass().getResource("backgroundImages/pokeballBackground.png")).getImage();
		labelOfBackgroundImage.setIcon(new ImageIcon(backgroundImage));
		
		frame.setVisible(true);
	}
	
	private void registerTrainer() {
		if(!trainerName.isEmpty() && !trainerEmail.isEmpty() && !selectedMonth.isEmpty() && !selectedDay.isEmpty() && 
		   !selectedYear.isEmpty() && !selectedGender.isEmpty() && !trainerPassword.isEmpty() && !trainerConfirmPassword.isEmpty()) {
			
			Trainer newTrainer = new Trainer(trainerName,
											 selectedMonth,
											 selectedDay,
											 selectedYear,
											 selectedGender,
											 trainerEmail,
											 trainerPassword);
	
			if(!newTrainer.verifyIfTrainerIsRegisteredInCSVFile()) {
				if(checkConfirmPassword()) {
					newTrainer.registerTreinerDataOnCSVFile();
					JOptionPane.showMessageDialog(frame, "Treiner successfully registered!");
					LoginInterface loginPage = new LoginInterface();
					loginPage.openPage();
					frame.setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(frame, "REGISTRATION FAIL!\nThe passwords are not equal!");
				}
			}
			else {
				JOptionPane.showMessageDialog(frame, "REGISTRATION FAIL!\nTreiner already registered!");
				LoginInterface login = new LoginInterface();
				login.openPage();
				frame.setVisible(false);
			}
		}
		else {
			JOptionPane.showMessageDialog(frame, "REGISTRATION FAIL!\nPlease, complete all fields!");
		}
	}
	
	private boolean checkConfirmPassword() {
		if(trainerPassword.intern() == trainerConfirmPassword.intern()) {
			return true;
		}
		return false;
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public void openPage() {
		initialize();
	}
}