import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

public class LoginInterface {

	private JFrame frame;
	private JLabel labelEmail;
	private JLabel labelPassword;
	private JTextField textFieldOfEmail;
	private JPasswordField passwordField;
	private JButton buttonLogIn;
	private JButton buttonRegister;
	private JButton buttonViewPokemons;
	private JLabel lblPokedex;
	private JLabel labelOfPokemonAnimation;
	private JLabel labelOfBackgroundImage;

	public LoginInterface() {
		
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(600, 600, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);

		labelOfPokemonAnimation = new JLabel("");
		labelOfPokemonAnimation.setBounds(201, 365, 200, 201);
		frame.getContentPane().add(labelOfPokemonAnimation);
		Image PokemonImage = new ImageIcon(this.getClass().getResource("backgroundImages/mew.gif")).getImage();
		labelOfPokemonAnimation.setIcon(new ImageIcon(PokemonImage));
		
		labelEmail = new JLabel("Email:");
		labelEmail.setFont(new Font("Noto Sans CJK SC Black", Font.PLAIN, 16));
		labelEmail.setBounds(201, 211, 63, 15);
		frame.getContentPane().add(labelEmail);
		
		labelPassword = new JLabel("Password:");
		labelPassword.setFont(new Font("Noto Sans CJK SC Black", Font.PLAIN, 16));
		labelPassword.setBounds(172, 245, 92, 20);
		frame.getContentPane().add(labelPassword);
		
		textFieldOfEmail = new JTextField();
		textFieldOfEmail.setBounds(273, 208, 160, 25);
		frame.getContentPane().add(textFieldOfEmail);
		textFieldOfEmail.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(273, 245, 160, 25);
		frame.getContentPane().add(passwordField);
		
		buttonLogIn = new JButton("Log In");
		buttonLogIn.setBackground(Color.pink.brighter());
		buttonLogIn.setFont(new Font("Noto Sans CJK TC Bold", Font.PLAIN, 13));
		buttonLogIn.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				String trainerEmail = textFieldOfEmail.getText();
				String trainerPassword = passwordField.getText();
				
				if(!trainerEmail.isEmpty() && !trainerPassword.isEmpty()) {
					String line;
					String csvSplitBy = ",";
					int emailIsRegistered = 0;
					
					try (BufferedReader  csvReader= new BufferedReader(new FileReader("data/csv_files/TRAINERS_DATA.csv"))) {
			            while ((line = csvReader.readLine()) != null) {
			            	String[] trainerAtributes = line.split(csvSplitBy);
			      
			            	if(trainerEmail.intern() == trainerAtributes[0].intern()) {
			            		if(trainerPassword.intern() == trainerAtributes[1].intern()) {
			            			
			            			Trainer loggedTrainer = new Trainer();
			            			
			            			loggedTrainer.setEmail(trainerAtributes[0]);
			            			loggedTrainer.setPassword(trainerAtributes[1]);
			            			loggedTrainer.setName(trainerAtributes[2]);
			            			loggedTrainer.setBirthdayMonth(trainerAtributes[3]);
			            			loggedTrainer.setBirthday(trainerAtributes[4]);
			            			loggedTrainer.setBirthdayYear(trainerAtributes[5]);
			            			loggedTrainer.setGender(trainerAtributes[6]);
			            			
			            			MainMenuInterface mainMenu = new MainMenuInterface();
			            			mainMenu.setTrainer(loggedTrainer);
			            			mainMenu.openPage();
			            			frame.setVisible(false);
			            			emailIsRegistered = 2;
			            		}
			            		else {
			            			emailIsRegistered = 1;
			            		}
			            		
			            	}
			            }
			            if(emailIsRegistered == 1) {
			            	JOptionPane.showMessageDialog(frame, "LOGIN FAIL!\nIncorrect password!");
			            }
			            else if(emailIsRegistered == 0){
			            	JOptionPane.showMessageDialog(frame, "LOGIN FAIL!\nUnregistered Trainer!");
			            }
			            
					}
					catch(IOException e1) {
				          e1.printStackTrace();
					}
				}
				else {
					JOptionPane.showMessageDialog(frame, "Please, complete all fields!");
				}
			}
				
				
		});
		buttonLogIn.setBounds(172, 291, 124, 25);
		frame.getContentPane().add(buttonLogIn);
		
		buttonRegister = new JButton("Register");
		buttonRegister.setBackground(Color.pink.brighter());
		buttonRegister.setFont(new Font("Noto Sans CJK SC Bold", Font.PLAIN, 13));
		buttonRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				TrainerRegisterInterface register = new TrainerRegisterInterface();
				register.openPage();
				frame.setVisible(false);
				
			}
		});
		buttonRegister.setBounds(308, 291, 125, 25);
		frame.getContentPane().add(buttonRegister);
		
		buttonViewPokemons = new JButton("View Pokemons");
		buttonViewPokemons.setBackground(Color.pink.brighter());
		buttonViewPokemons.setFont(new Font("Noto Sans CJK KR Bold", Font.BOLD, 13));
		buttonViewPokemons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokemonSearchInterface pokemonSearchScreen = new PokemonSearchInterface("LOGIN");
				PokemonSearchInterface.setTrainer(null);
				pokemonSearchScreen.openPage();
				frame.setVisible(false);
			}
		});
		buttonViewPokemons.setBounds(172, 328, 261, 25);
		frame.getContentPane().add(buttonViewPokemons);
		
		lblPokedex = new JLabel("POKEDEX");
		lblPokedex.setFont(new Font("Noto Sans CJK SC Black", Font.BOLD, 50));
		lblPokedex.setBounds(177, 120, 363, 45);
		frame.getContentPane().add(lblPokedex);
		
		labelOfBackgroundImage = new JLabel("");
		labelOfBackgroundImage.setBounds(0, 0, 600, 600);
		frame.getContentPane().add(labelOfBackgroundImage);
		Image backgroundImage = new ImageIcon(this.getClass().getResource("backgroundImages/pokeballBackground.png")).getImage();
		labelOfBackgroundImage.setIcon(new ImageIcon(backgroundImage));
		
		frame.setVisible(true);
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public void openPage() {
		initialize();
	}
}